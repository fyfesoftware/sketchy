﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core.Decorators
{
    /// <summary>
    /// The drop decorator represents a decorator that is used to illustrate the fact that one shape can be
    /// dropped onto another
    /// </summary>
    public class DropDecorator : IDecorator
    {
        #region IDecorator Members

        /// <summary>
        /// The shape that this decorator decorates
        /// </summary>
        private IDecoratableShape m_decorates;

        /// <summary>
        /// Gets or Sets the shape that this decorator decorates
        /// </summary>
        public IDecoratableShape Decorates
        {
            get { return m_decorates; }
            set
            {
                m_decorates = value;
                if (value != null && value.Site != null ) // We need to register as a component
                    value.Site.Container.Add(this, String.Format("{0}_decorator", value.Site.Name));
            }
        }

        #endregion

        #region IDrawable Members

        /// <summary>
        /// Draw this decorator to <paramref name="g"/>
        /// </summary>
        public bool DrawTo(System.Drawing.Graphics g)
        {

            float zoom = Decorates.GetCanvas().Zoom;

            Pen pRect = new Pen(Color.Red, 2f);
            // Set the position and size
            PointF position = new PointF(Decorates.DrawPosition.X, Decorates.DrawPosition.Y);
            SizeF size = new SizeF(Decorates.DrawSize.Width, Decorates.DrawSize.Height);

            // Now we want to correct the position and size
            position.X -= 3f; position.Y -= 3f;
            size.Width += 6f; size.Height += 6f;

            g.DrawRectangle(pRect, position.X, position.Y, size.Width, size.Height);
            return true;
        }

        #endregion

        #region IComponent Members

        /// <summary>
        /// Event is raised when this component is disposed
        /// </summary>
        public event EventHandler Disposed;

        /// <summary>
        /// Gets or sets the site of this component
        /// </summary>
        public System.ComponentModel.ISite Site { get; set; }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose this decorator
        /// </summary>
        public void Dispose()
        {
            // Un-register from the container site (if possible)
            if (this.Site != null)
                this.Site.Container.Remove(this);

            if (Disposed != null)
                Disposed(this, EventArgs.Empty);
        }

        #endregion
    }
}

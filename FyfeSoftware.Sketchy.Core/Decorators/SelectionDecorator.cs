﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core;
using FyfeSoftware.Sketchy.Core.Primitives;
using System.Drawing;
using FyfeSoftware.Sketchy.Core.Collections;

namespace FyfeSoftware.Sketchy.Core.Decorators
{

    /// <summary>
    /// A selection decorator is a special decorator that outlines the current object
    /// with a dashed line and grab handles
    /// </summary>
    public class SelectionDecorator : IDecorator, IMouseInteractiveShape
    {

        /// <summary>
        /// Gets or sets a pointer to the shape collection that contains all the selected objects 
        /// </summary>
        public ShapeCollection SiblingSelected { get; set; }

        /// <summary>
        /// Identifies the state of the size decorator
        /// </summary>
        public enum SizeState
        {
            Idle,
            Resizing,
            Cancel
        }

        /// <summary>
        /// Gets or sets the additional handles that should be drawn for this decorator
        /// </summary>
        private List<IConnector> m_connectorLines = new List<IConnector>();

        /// <summary>
        /// Currently editing connector line
        /// </summary>
        private IShape m_currentEditing = null;

        /// <summary>
        /// The current size grip
        /// </summary>
        public enum CurrentSizeGrip
        {
            /// <summary>
            /// No size grip
            /// </summary>
            None = 8,
            /// <summary>
            /// Top left size grip
            /// </summary>
            TopLeft = 0, 
            /// <summary>
            /// Top size grip
            /// </summary>
            Top = 1, 
            /// <summary>
            /// Top right size grip
            /// </summary>
            TopRight = 2,
            /// <summary>
            /// Middle left size grip
            /// </summary>
            Left = 3, 
            /// <summary>
            /// Middle right size grip
            /// </summary>
            Right = 4,
            /// <summary>
            /// Bottom left size grip
            /// </summary>
            BotLeft = 5,
            /// <summary>
            /// Bottom size grip
            /// </summary>
            Bot = 6,
            /// <summary>
            /// Bottom right size grip
            /// </summary>
            BotRight = 7,
            /// <summary>
            /// Connector size grip
            /// </summary>
            Connector = 9
        }

        /// <summary>
        /// Grip handles
        /// </summary>
        private List<RectangleF> m_gripHandles;

        /// <summary>
        /// The current state of the decorator (moving, idle, etc...)
        /// </summary>
        /// TODO: Create an enum for this
        private SizeState m_currentState = 0;

        /// <summary>
        /// Current sizing direction
        /// </summary>
        private CurrentSizeGrip m_currentSizeGrip = CurrentSizeGrip.None;

        /// <summary>
        /// The point where the mouse was first depressed 
        /// </summary>
        /// <remarks>Relative to the client</remarks>
        private PointF m_mouseDownAt;

        /// <summary>
        /// The original position of the decorated shape
        /// </summary>
        private PointF m_decoratesOriginalPosition;

        /// <summary>
        /// The current drop shape
        /// </summary>
        private IDroppableShape m_currentDropShape;

        /// <summary>
        /// The shape that this decorator decorates
        /// </summary>
        private IDecoratableShape m_decorates;

        /// <summary>
        /// Add a grip to 
        /// </summary>
        /// <param name="shape"></param>
        public void AddConnectorLine(IConnector shape)
        {
            if (m_connectorLines == null) m_connectorLines = new List<IConnector>();

            if(SiblingSelected == null || SiblingSelected.Count == 1)
                m_connectorLines.Add(shape);
        }

        /// <summary>
        /// Draw this decorator to the specified graphics surface
        /// </summary>
        public bool DrawTo(System.Drawing.Graphics g)
        {
            // Draw the dashed line outside of the decorates object
            if (Decorates == null) return false;

            // Set the position and size
            float zoom = Decorates.GetCanvas().Zoom;

            PointF position = new PointF(Decorates.DrawPosition.X, Decorates.DrawPosition.Y);
            SizeF size = new SizeF(Decorates.DrawSize.Width, Decorates.DrawSize.Height);

            // Now we want to correct the position and size
            position.X -= 2f; position.Y -= 2f;
            size.Width += 4f; size.Height += 4f;

            // Setup the pen for the dashed box
            Pen pDashed = new Pen(Color.Green, 1);
            pDashed.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            // Draw the box
            g.DrawRectangle(pDashed, position.X, position.Y, size.Width, size.Height);

            // Create the grip handles
            m_gripHandles = new List<RectangleF>();
            for (int rHandle = 0; rHandle < 3; rHandle++) // row handles
                for (int cHandle = 0; cHandle < 3; cHandle++)  // col handles
                {
                    // Calculate the rectangle
                    if (cHandle == 1 && rHandle == 1)
                        continue; // No centre handle

                        float x = position.X + cHandle * (size.Width / 2f),
                            y = position.Y + rHandle * (size.Height / 2f);
                        m_gripHandles.Add(new RectangleF(x - 3f, y - 3f, 6f, 6f));
                    //}
                }
            
            // Add the additional grip handles
            foreach (IShape shp in m_connectorLines)
            {
                m_gripHandles.Add(new RectangleF(shp.DrawPosition.X - 3f, shp.DrawPosition.Y - 3f, 6, 6));
                PointF botRight = new PointF(shp.DrawPosition.X + shp.DrawSize.Width, shp.DrawPosition.Y + shp.DrawSize.Height);
                m_gripHandles.Add(new RectangleF(botRight.X - 3f, botRight.Y - 3f, 6, 6));
            }

            // Draw the grip handles
            foreach(RectangleF r in m_gripHandles)
            {

                CurrentSizeGrip csg = CurrentSizeGrip.Connector;
                if(m_gripHandles.IndexOf(r) < 8)
                    csg = (CurrentSizeGrip)m_gripHandles.IndexOf(r);

                if ((Decorates.AllowedSizing & SizeModeType.MoveOnly) != 0)
                    continue;

                // Allowed to resize?
                if ((Decorates.AllowedSizing & SizeModeType.Horizontal) == 0 &&
                    (csg == CurrentSizeGrip.Left || csg == CurrentSizeGrip.Right))
                    g.FillRectangle(Brushes.Red, r);
                else if ((Decorates.AllowedSizing & SizeModeType.Vertical) == 0 &&
                    (csg == CurrentSizeGrip.Bot || csg == CurrentSizeGrip.Top))
                    g.FillRectangle(Brushes.Red, r);
                else if(csg == CurrentSizeGrip.Connector)
                    g.FillRectangle(Brushes.Yellow, r);
                else if ((Decorates.AllowedSizing & SizeModeType.Horizontal) == 0 && (Decorates.AllowedSizing & SizeModeType.Vertical) == 0)
                    g.FillRectangle(Brushes.Red, r);
                else
                    g.FillRectangle(Brushes.LightGreen, r);

                g.DrawRectangle(Pens.Green, r.X, r.Y, r.Width, r.Height);
            }

            return true;
        }

        #region IDecorator Members

        /// <summary>
        /// Gets or Sets the shape that this decorator decorates
        /// </summary>
        public IDecoratableShape Decorates
        {
            get { return m_decorates; }
            set
            {
                m_decorates = value;
                if (value != null && value.Site != null) // We need to register as a component
                    value.Site.Container.Add(this, String.Format("{0}_decorator", value.Site.Name));
                m_decoratesOriginalPosition = value.Position;
            }
        }

        #endregion

        #region IMouseInteractiveShape Members

        /// <summary>
        /// False if this item is not in the hit test
        /// </summary>
        public bool HitTest(System.Drawing.Point p)
        {

            // Zoom hist test
            float zoom = Decorates.GetCanvas().Zoom;

            // No grip handles? we can select
            if (m_gripHandles == null && this.Decorates is IMouseInteractiveShape)
            {
                this.m_currentState = SizeState.Resizing;
                return (this.Decorates as IMouseInteractiveShape).HitTest(p);
            }
            else if (this.m_gripHandles == null)
                return false;
            else if (m_currentState != SizeState.Idle) // resizing
                return true;

            bool hits = false;
            m_currentSizeGrip = CurrentSizeGrip.None;

            // Grip handles
            foreach (RectangleF r in m_gripHandles)
                if(r.Contains(p))
                {
                    m_currentSizeGrip = m_gripHandles.IndexOf(r) >= 9 ? CurrentSizeGrip.Connector : (CurrentSizeGrip)m_gripHandles.IndexOf(r);
                    hits = true;
                }

            // If we didn't hit anything, maybe we're moving?
            if (!hits)
            {
                RectangleF innerBound = new RectangleF(Decorates.DrawPosition.X, Decorates.DrawPosition.Y, Decorates.DrawSize.Width, Decorates.DrawSize.Height),
                    outerBound = new RectangleF(Decorates.DrawPosition.X - 3, Decorates.DrawPosition.Y - 3, Decorates.DrawSize.Width + 6, Decorates.DrawSize.Height + 6);
                if (outerBound.Contains(p)) // && !innerBound.Contains(p))
                {
                    hits = true;
                    m_currentSizeGrip = CurrentSizeGrip.None;
                }
            }

            // Move 
            return hits;
        }

        /// <summary>
        /// Handle the mouse leaving the selection decorator area
        /// </summary>
        public virtual void HandleMouseLeave(System.Drawing.Point p, MouseKeys mouseKeys)
        {
        }

        /// <summary>
        /// Handle the mouse entering the selection decorator area
        /// </summary>
        public virtual void HandleMouseEnter(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            m_currentState = SizeState.Idle;
        }

        #endregion

        #region IMouseInteractive Members

        /// <summary>
        /// Handle the mouse clicking on the selection decorator area
        /// </summary>
        public void HandleMouseClick(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            if (this.Decorates is IMouseInteractiveShape)
                (this.Decorates as IMouseInteractiveShape).HandleMouseClick(p, mouseKeys);
            
        }

        /// <summary>
        /// Handle a mouse key being depressed
        /// </summary>
        public void HandleMouseDown(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            float zoom = Decorates.GetCanvas().Zoom;

            m_currentState = SizeState.Resizing;
            m_mouseDownAt = Decorates.PointToClientZoom(p);

            // Determine where the mouse went down
            m_currentEditing = Decorates;
            foreach (IShape shp in m_connectorLines)
            {
                Point connPoint = shp.PointToClientZoom(p);
                RectangleF botRightRect = new RectangleF(shp.Size.Width - 3, shp.Size.Height - 3, 6, 6);
                if (botRightRect.Contains(connPoint))
                    m_currentEditing = shp;
            }

        }

        /// <summary>
        /// Handle a mouse up event
        /// </summary>
        public void HandleMouseUp(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            // Reset the original position
            if (m_currentState == SizeState.Cancel)
                Decorates.Position = m_decoratesOriginalPosition;
            else if(m_currentSizeGrip == CurrentSizeGrip.None || m_currentSizeGrip == CurrentSizeGrip.Connector)
            {
                m_decoratesOriginalPosition = Decorates.Position;

                // if the current drop shape is not null, dispose of its decorator
                if (m_currentDropShape != null)
                {
                    PointF dropPoint = m_currentDropShape.PointToClientZoom(m_decoratesOriginalPosition);
                    m_currentDropShape.HandleDrop(m_currentEditing, new Point((int)dropPoint.X, (int)dropPoint.Y));
                    // Set dispose of the drop shape's decorator
                    if (m_currentDropShape is IInteractiveShape)
                        (m_currentDropShape as IInteractiveShape).Decorator = null;
                }
               
            }

            // Connector stuff
            IConnector connectorDecorates = Decorates as IConnector;
            if (connectorDecorates != null)
                connectorDecorates.RecalculatePosition();

            // Idle
            m_currentState = SizeState.Idle;

            // Call update
            Decorates.OnRedrawRequired();
            
        }

        /// <summary>
        /// Handle a mouse move event
        /// </summary>
        public virtual void HandleMouseMove(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            // Now resize the shape
            if (m_currentState != SizeState.Idle)
            {
                ResizeDecoratesObject(p);
            }
            else if (this.Decorates is IMouseInteractiveShape)
                (this.Decorates as IMouseInteractiveShape).HandleMouseMove(p, mouseKeys);


        }

        /// <summary>
        /// Resize the object that this decorator decorates
        /// </summary>
        private void ResizeDecoratesObject(Point p)
        {

            // Zoom factor
            PointF clientPoint = Decorates.PointToClientZoom(p);

            // Determine a dw, dh, dx, and dy %age
            float dx = 0,
                dy = 0,
                dw = 0,
                dh = 0;

            // Determine how to size this item
            switch (m_currentSizeGrip)
            {
                case CurrentSizeGrip.Top:
                    dy = clientPoint.Y;
                    dh = -clientPoint.Y;
                    break;
                case CurrentSizeGrip.Left:
                    dw = -clientPoint.X;
                    dx = clientPoint.X;
                    break;
                case CurrentSizeGrip.Right:
                    dw = -Decorates.Size.Width + clientPoint.X;
                    break;
                case CurrentSizeGrip.Bot:
                    dh = -Decorates.Size.Height + clientPoint.Y;
                    break;
                case CurrentSizeGrip.BotRight:
                    dh = -Decorates.Size.Height + clientPoint.Y;
                    dw = -Decorates.Size.Width + clientPoint.X;
                    break;
                case CurrentSizeGrip.BotLeft:
                    dx = clientPoint.X;
                    dw = -clientPoint.X;
                    dh = -Decorates.Size.Height + clientPoint.Y;
                    break;
                case CurrentSizeGrip.TopLeft:
                    dy = clientPoint.Y;
                    dh = -clientPoint.Y;
                    dw = -clientPoint.X;
                    dx = clientPoint.X;
                    break;
                case CurrentSizeGrip.TopRight:
                    dy = clientPoint.Y;
                    dh = -clientPoint.Y;
                    dw = -Decorates.Size.Width + clientPoint.X;
                    break;
                case CurrentSizeGrip.None: // move
                    PointF deltaPoint = new PointF(clientPoint.X - m_mouseDownAt.X , clientPoint.Y - m_mouseDownAt.Y);
                    dx = deltaPoint.X;
                    dy = deltaPoint.Y;
                    break;
                case CurrentSizeGrip.Connector: // Move connector
                    // Which connector are we on?
                    var mcePoint = m_currentEditing.PointToClientZoom(p);
                    dh = -m_currentEditing.Size.Height + mcePoint.Y;
                    dw = -m_currentEditing.Size.Width + mcePoint.X;
                    break;
            }

            // Resize / move everything
            var toEdit = SiblingSelected;
            if (m_currentEditing != Decorates)
                toEdit = new ShapeCollection() { m_currentEditing };

            foreach (IShape shp in toEdit)
            {
                float tdw = dw, tdh = dh, tdx = dx, tdy = dy;

                // Apply lock rules
                if (m_currentSizeGrip != CurrentSizeGrip.None && (shp.AllowedSizing & SizeModeType.Horizontal) == 0)
                {
                    tdw = 0;
                    tdx = 0;
                }
                if (m_currentSizeGrip != CurrentSizeGrip.None && (shp.AllowedSizing & SizeModeType.Vertical) == 0)
                {
                    tdh = 0;
                    tdy = 0;
                }
                if (m_currentSizeGrip == CurrentSizeGrip.None && (shp.AllowedSizing == SizeModeType.None))
                {
                    tdx = 0;
                    tdy = 0;
                }

                shp.Size = new SizeF(shp.Size.Width + tdw, shp.Size.Height + tdh);
                shp.Position = new PointF(shp.Position.X + tdx, shp.Position.Y + tdy);

            }

            // Handle any drag events
            CallDragEvents(p);

            // On redraw is required
            Decorates.OnRedrawRequired();
        }


        /// <summary>
        /// Call any drag events on this object
        /// </summary>
        private void CallDragEvents(Point p)
        {
            // Get the canvas for the decorated object
            ICanvas hostCanvas = Decorates.GetCanvas();
            if (hostCanvas == null || (m_currentSizeGrip != CurrentSizeGrip.None &&
                m_currentSizeGrip != CurrentSizeGrip.Connector)) return;
            
            // Get the droppable item at this position
            var oldDropShape = m_currentDropShape;
            m_currentDropShape = hostCanvas.FindShapeAt(p, shp => shp is IDroppableShape && shp != Decorates && !SiblingSelected.Contains(shp)) as IDroppableShape;
            var interactiveDropShape = m_currentDropShape as IInteractiveShape;

            // Reset state
            m_currentState = SizeState.Resizing;

            // reset the decorator shape
            if (oldDropShape != m_currentDropShape && oldDropShape is IInteractiveShape)
                (oldDropShape as IInteractiveShape).Decorator = null;
            
            // Current drop shape is null
            if (m_currentDropShape == null) return;

            // Query the droppable shape
            switch (m_currentDropShape.HandleDragOver(m_currentEditing, m_currentDropShape.PointToClientZoom(p)))
            {
                case DragEffect.Allowed:
                    if (interactiveDropShape != null && m_currentDropShape != oldDropShape)
                    {
                        interactiveDropShape.Decorator = new DropDecorator();
                        hostCanvas.SendShapeToBack(interactiveDropShape.Decorator as IShape);
                    }
                    break;
                case DragEffect.NotAllowed:
                    m_currentState = SizeState.Cancel;
                    m_currentDropShape = null;
                    break;
                case DragEffect.NotSupported:
                    if (interactiveDropShape != null && m_currentDropShape != oldDropShape)
                        interactiveDropShape.Decorator = null;
                         
                    m_currentDropShape = null;
                    break;
            }

        }

        /// <summary>
        /// Handle double click
        /// </summary>
        public void HandleDoubleClick(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            if (this.Decorates is IMouseInteractiveShape)
                (this.Decorates as IMouseInteractiveShape).HandleDoubleClick(p, mouseKeys);
            
        }

        /// <summary>
        /// Handle the dwell
        /// </summary>
        /// <param name="p"></param>
        /// <param name="mouseKeys"></param>
        /// <returns></returns>
        public void HandleMouseDwell(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            if (this.Decorates is IMouseInteractiveShape)
                (this.Decorates as IMouseInteractiveShape).HandleMouseDwell(p, mouseKeys);
            
        }

        #endregion

        #region IComponent Members

        /// <summary>
        /// Fired when this item is disposed
        /// </summary>
        public event EventHandler Disposed;

        /// <summary>
        /// Gets or sets the site that this decorator is comprised of
        /// </summary>
        public System.ComponentModel.ISite Site { get; set; }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose this object from memory
        /// </summary>
        public void Dispose()
        {
            // Un-register from the container site (if possible)
            if (this.Site != null)
                this.Site.Container.Remove(this);

            if(m_gripHandles != null)
                m_gripHandles.Clear();

            if (Disposed != null)
                Disposed(this, EventArgs.Empty);
        }

        #endregion
    }
}

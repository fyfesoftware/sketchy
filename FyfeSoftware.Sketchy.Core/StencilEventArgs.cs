﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// A common class that stores data about shape editing events that occur
    /// within a canvas
    /// </summary>
    public class StencilEventArgs : EventArgs
    {

        /// <summary>
        /// Identifies the stencil that was updated
        /// </summary>
        public IStencil Stencil { get; set; }
    }
}

﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using FyfeSoftware.Sketchy.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// A shape which draws an image
    /// </summary>
    public class ImageShape : AbstractShape
    {

        // The image
        private Image m_image;

        /// <summary>
        /// When true indicates the image should grow with the container
        /// </summary>
        public Boolean AutoScale { get; set; }

        /// <summary>
        /// Gets or sets the image attached to the shape
        /// </summary>
        public Image Image {
            get
            {
                return this.m_image;
            } 
            set
            {
                this.m_image = value;
                if(this.Image != null)
                    this.Size = this.Image.Size;
            }
        }

        /// <summary>
        /// Draw the shape
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {
            if(this.Image != null && this.AutoScale)
                g.DrawImage(this.Image, this.DrawPosition.X, this.DrawPosition.Y, this.DrawSize.Width, this.DrawSize.Height);
            else if (this.Image != null)
                g.DrawImage(this.Image, this.DrawPosition.X, this.DrawPosition.Y, this.Image.Size.Width, this.Image.Size.Height);
            return true;
        }
    }
}

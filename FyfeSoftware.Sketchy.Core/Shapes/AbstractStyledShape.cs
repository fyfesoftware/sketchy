﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core;
using FyfeSoftware.Sketchy.Core.Primitives;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// Represents a shape that can be styled in a custom way on the canvas
    /// </summary>
    public abstract class AbstractStyledShape : AbstractShape
    {

        /// <summary>
        /// The outline colour of the shape
        /// </summary>
        private Color m_outlineColor;
        /// <summary>
        /// The fill brush of the shape
        /// </summary>
        private Brush m_fillBrush;
        /// <summary>
        /// The outline style of the shape
        /// </summary>
        private DashStyle m_outlineStyle;
        /// <summary>
        /// The outline width of the shape
        /// </summary>
        private float m_outlineWidth;
        /// <summary>
        /// The brush to use for the shadow
        /// </summary>
        private Brush m_shadowBrush;

        /// <summary>
        /// Gets or sets the brush to use to draw the shadow
        /// </summary>
        public Brush ShadowBrush { get { return m_shadowBrush; } set { m_shadowBrush = value; OnRedrawRequired(); } }
        /// <summary>
        /// Gets or sets the colour of the outline
        /// </summary>
        public Color OutlineColor { get { return m_outlineColor; } set { m_outlineColor = value; OnRedrawRequired(); } }
        /// <summary>
        /// Gets or sets the colour of the fill of the object
        /// </summary>
        public Brush FillBrush { get { return m_fillBrush; } set { m_fillBrush = value; OnRedrawRequired(); } }
        /// <summary>
        /// Gets or sets the outline style of the object
        /// </summary>
        public DashStyle OutlineStyle { get { return m_outlineStyle; } set { m_outlineStyle = value; OnRedrawRequired(); } }
        /// <summary>
        /// Gets or sets the outline width of the object
        /// </summary>
        public float OutlineWidth { get { return m_outlineWidth; } set { m_outlineWidth = value; OnRedrawRequired(); } }
        
    }
}

﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using FyfeSoftware.Sketchy.Core;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// The arrow shape represents a simple arrow head on a line shape
    /// </summary>
    public class ConnectionLineShape : AbstractStyledShape, IConnector
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ConnectionLineShape()
        {
            this.ArrowType = LineCap.NoAnchor;
        }

        /// <summary>
        /// Define a 10 pixel arrow head height
        /// </summary>
        private const int ARROW_HEIGHT = 10;

        /// <summary>
        /// The arrow type
        /// </summary>
        public LineCap ArrowType { get; set; }

        /// <summary>
        /// Source shape of the connection
        /// </summary>
        private IShape m_sourceShape;

        /// <summary>
        /// Target shape of the connection
        /// </summary>
        private IShape m_targetShape;

        /// <summary>
        /// The decorator that decorates this shape
        /// </summary>
        private IDecorator m_decorator;

        /// <summary>
        /// Fired when the source or target of this connector changes
        /// </summary>
        public event EventHandler ConnectionChanged;

        /// <summary>
        /// Draw this arrow head to the graphics canvas
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {
            // We start the arrow at position and draw to position + size 


            // We will need to calculate the x,y position where the arrow head starts
            // this is done by calculating ratio of the hypotinuses of two right angle
            // triangles
            //double lineLength = Math.Sqrt(Math.Pow(Size.Width, 2.0f) + Math.Pow(Size.Height, 2.0f));
            //double hypRatio = (float)ARROW_HEIGHT / lineLength;

            PointF arrowHeadPoint = new PointF(DrawPosition.X + DrawSize.Width, DrawPosition.Y + DrawSize.Height);
            //    arrowHeadStartPos = new PointF((float)(arrowHeadPoint.X - Size.Width * hypRatio), (float)(arrowHeadPoint.Y - Size.Height * hypRatio));

            Pen p = new Pen(OutlineColor, OutlineWidth);
            p.DashStyle = OutlineStyle;
            p.EndCap = this.ArrowType;
            
            // Draw to the canvas 
            g.DrawLine(p, DrawPosition, arrowHeadPoint);

            if(ShadowBrush != null)
                g.DrawLine(new Pen(ShadowBrush, OutlineWidth) { EndCap = LineCap.ArrowAnchor }, DrawPosition.X + 2, DrawPosition.Y + 3, arrowHeadPoint.X + 2, arrowHeadPoint.Y + 3);

            return true;

        }

        #region IConnector Members

        /// <summary>
        /// Gets or sets the target of this connector
        /// </summary>
        public IShape Source
        {
            get
            {
                return m_sourceShape;
            }
            set
            {
                if (m_sourceShape != null) // unbind 
                {
                    m_sourceShape.PositionChanged -= new EventHandler(m_sourceShape_PositionChanged);
                    m_sourceShape.SizeChanged -= new EventHandler(m_sourceShape_PositionChanged);
                }

                m_sourceShape = value;
                if (m_sourceShape != null) // bind
                {
                    m_sourceShape.PositionChanged += new EventHandler(m_sourceShape_PositionChanged);
                    m_sourceShape.SizeChanged += new EventHandler(m_sourceShape_PositionChanged);
                    this.UpdateSizeAndPosition();
                }

                // Fire the event
                if (ConnectionChanged != null)
                    ConnectionChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets the source of this connector
        /// </summary>
        public IShape Target
        {
            get
            {
                return m_targetShape;
            }
            set
            {
                if (m_targetShape != null) // unbind 
                {
                    m_targetShape.PositionChanged -= new EventHandler(m_sourceShape_PositionChanged);
                    m_targetShape.SizeChanged -= new EventHandler(m_sourceShape_PositionChanged);
                }

                m_targetShape = value;
                if (m_targetShape != null) // bind
                {
                    m_targetShape.PositionChanged += new EventHandler(m_sourceShape_PositionChanged);
                    m_targetShape.SizeChanged += new EventHandler(m_sourceShape_PositionChanged);
                    this.UpdateSizeAndPosition();
                }

                // Fire the event
                if (ConnectionChanged != null)
                    ConnectionChanged(this, EventArgs.Empty);

            }
        }

        /// <summary>
        /// Position has changed
        /// </summary>
        void m_sourceShape_PositionChanged(object sender, EventArgs e)
        {
            this.UpdateSizeAndPosition();
        }

        /// <summary>
        /// Update Size and position
        /// </summary>
        private void UpdateSizeAndPosition()
        {
            // Bind the start
            if (Source == null) return;
            Position = new PointF(Source.Position.X + Source.Size.Width / 2, Source.Position.Y + Source.Size.Height / 2);

            // Recalc size
            if (Target == null) return;
            PointF targetCentre = new PointF(Target.Position.X + Target.Size.Width / 2, Target.Position.Y + Target.Size.Height / 2);
            SizeF lineBoundBox = new SizeF(targetCentre.X - Position.X, targetCentre.Y - Position.Y);
            // Now calculate the point where the line will touch the surface of the source
            Size = lineBoundBox; //new SizeF(lineBoundBox.Width - Source.Size.Width / 2, lineBoundBox.Height - newHeight);
        }

        /// <summary>
        /// Gets or sets the direction this connector is facing
        /// </summary>
        public DirectionType Direction { get; set; }

        /// <summary>
        /// Determine if the connection is permitted
        /// </summary>
        public bool IsConnectionAllowed(IShape source, IShape target)
        {
            return target != null || source != null;
        }

        /// <summary>
        /// Recalculate this position
        /// </summary>
        public void RecalculatePosition()
        {
            m_sourceShape_PositionChanged(this, EventArgs.Empty);
        }

        #endregion

    }
}

﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using FyfeSoftware.Sketchy.Core;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// A shape the represents a closed arrow
    /// </summary>
    public class ClosedArrowShape : AbstractStyledShape
    {

        /// <summary>
        /// Gets or sets the direction of this item
        /// </summary>
        public DirectionType Direction { get; set; }

        /// <summary>
        /// Draw this shape to the graphics
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {

            // Handle zoom and size / position
            float zoom = GetCanvas().Zoom;
            float radius = (DrawSize.Height) / 2;
            PointF position = DrawPosition;
            SizeF size = DrawSize;

            // Create the brushes
            Pen outlinePen = new Pen(base.OutlineColor, base.OutlineWidth);
            outlinePen.DashStyle = base.OutlineStyle;
            Brush backBrush = base.FillBrush, shadowBrush = base.ShadowBrush;

            // Create the path
            GraphicsPath path = new GraphicsPath();

            switch(Direction)
            {
                case DirectionType.East:
                    path.AddLine(position, new PointF(position.X + size.Width - 2 * radius, position.Y));
                    path.AddArc(position.X + size.Width - 2 * radius, position.Y, radius * 2, radius * 2, 270, 180);
                    path.AddLine(position.X + size.Width - 2 * radius, position.Y + size.Height, position.X, position.Y + size.Height);
                    path.AddLine(position.X, position.Y + size.Height, position.X, position.Y);
                    break;
                case DirectionType.West:
                    path.AddLine(position.X + radius, position.Y, position.X + size.Width, position.Y);
                    path.AddLine(position.X + size.Width, position.Y, position.X + size.Width, position.Y + size.Height);
                    path.AddLine(position.X + size.Width, position.Y + size.Height, position.X + 2 * radius, position.Y + size.Height);
                    path.AddArc(position.X, position.Y, radius * 2, radius * 2, 90, 180);
                    break;

            }

            if (shadowBrush != null)
            {
                var shadowPath = path.Clone() as GraphicsPath;
                Matrix translateMatrix = new Matrix();
                translateMatrix.Translate(2, 3);
                shadowPath.Transform(translateMatrix);
                g.FillPath(shadowBrush, shadowPath);
            }
            if (backBrush != null)
                g.FillPath(backBrush, path);

            g.DrawPath(outlinePen, path);

            return true;
        }
    }
}

﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// Represents a single recangle 
    /// </summary>
    public class RectangleShape : AbstractStyledShape
    {
        /// <summary>
        /// Draw the rectangle shape to the specified graphics 
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {
            Pen outlinePen = new Pen(base.OutlineColor, base.OutlineWidth);
            outlinePen.DashStyle = base.OutlineStyle;
            Brush backBrush = base.FillBrush;
            Brush shadowBrush = base.ShadowBrush;

            // Handle zoom and size / position
            float zoom = GetCanvas().Zoom;
            PointF position = DrawPosition;
            SizeF size = DrawSize;

            // Create the path
            RectangleF path = new RectangleF(position.X, position.Y, size.Width, size.Height);

            if (shadowBrush != null)
            {
                var shadowPath = new RectangleF(position.X + 2, position.Y + 3, size.Width, size.Height);
                Matrix translateMatrix = new Matrix();
                g.FillRectangle(shadowBrush, shadowPath);
            }
            if (backBrush != null)
                g.FillRectangle(backBrush, path);

            g.DrawRectangle(outlinePen, path.X, path.Y, path.Width, path.Height);

            return true;

        }
    }
}

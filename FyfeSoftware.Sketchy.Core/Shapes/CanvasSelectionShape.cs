﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core;
using FyfeSoftware.Sketchy.Core.Primitives;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// The canvas selection shape represents a bounding box on the user's screen that indicates
    /// the current selection area for the item.
    /// </summary>
    public class CanvasSelectionShape : AbstractShape
    {

        /// <summary>
        /// Gets the selection binding box
        /// </summary>
        public RectangleF BindingBox { get; private set; }

        /// <summary>
        /// Gets or sets the size and calls the appropriate draw event
        /// </summary>
        public override SizeF Size { get { return base.Size; } set { 
            base.Size = value; OnRedrawRequired(); } }

        /// <summary>
        /// Creates a new instance of the canvas selection shape
        /// </summary>
        public CanvasSelectionShape(Point position)
        {
            this.Position = position;
            this.Size = new SizeF(0, 0);
        }

        /// <summary>
        /// Gets the size this item is drawn (zoom does not apply)
        /// </summary>
        public override SizeF DrawSize
        {
            get
            {
                return Size;
            }
        }

        /// <summary>
        /// Gets the position this item is drawn at (zoom does not apply)
        /// </summary>
        public override PointF DrawPosition
        {
            get
            {
                return Position;
            }
        }

        /// <summary>
        /// Draw this shape to the specified graphics port
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {
            Pen p = new Pen(SystemBrushes.ControlDark);
            p.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;
            p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            p.Width = 1.0f;

            // Get zoom
            float zoom = GetCanvas().Zoom;
            PointF startPosition = Position;
            SizeF realSize =  Size;

            // Calculate the real position and height/width
            if (realSize.Height < 0)
            {
                startPosition.Y += realSize.Height;
                realSize.Height = -realSize.Height;
            }
            if (realSize.Width < 0)
            {
                startPosition.X += realSize.Width;
                realSize.Width = -realSize.Width;
            }

            // Position and selection
            BindingBox = new RectangleF(startPosition.X / zoom, startPosition.Y / zoom, realSize.Width / zoom, realSize.Height / zoom);
            
            // Draw the binding box
            g.DrawRectangle(p, startPosition.X, startPosition.Y, realSize.Width, realSize.Height);

            return true;

        }
    }
}

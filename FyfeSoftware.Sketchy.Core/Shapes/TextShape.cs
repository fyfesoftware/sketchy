﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core.Shapes
{
    /// <summary>
    /// The text shape represents a text area on the screen.
    /// </summary>
    public class TextShape : AbstractStyledShape
    {
        /// <summary>
        /// The text of this shape
        /// </summary>
        private string m_text;
        /// <summary>
        /// String alignment
        /// </summary>
        private StringAlignment m_stringAlignment = StringAlignment.Center;

        /// <summary>
        /// Gets or sets the text of the object
        /// </summary>
        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;

                // Measure the string
                Bitmap bmp = new Bitmap(1, 1);
                Graphics g = Graphics.FromImage(bmp);
                Size = g.MeasureString(Text, Font ?? SystemFonts.CaptionFont);

            }
        }

        /// <summary>
        /// Font data for the object
        /// </summary>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the string alignment
        /// </summary>
        public StringAlignment Alignment
        {
            get { return m_stringAlignment; }
            set
            {
                m_stringAlignment = value;
                OnRedrawRequired();
            }
        }

        /// <summary>
        /// Draw this text to the specific graphics position
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {
            
            // First off, setup our draw
            StringFormat strF = new StringFormat();
            strF.Alignment = Alignment;
            strF.FormatFlags = StringFormatFlags.NoWrap;

            // Handle zoom and size / position
            float zoom = GetCanvas().Zoom;
            PointF position = DrawPosition;
            SizeF size = DrawSize;
            Font font = new Font((Font ?? SystemFonts.CaptionFont).FontFamily, (Font ?? SystemFonts.CaptionFont).Size * zoom, (Font ?? SystemFonts.CaptionFont).Style);

            g.DrawString(Text, font ?? SystemFonts.CaptionFont, FillBrush, new RectangleF(position, size), strF);
            return true;
        }
    }
}

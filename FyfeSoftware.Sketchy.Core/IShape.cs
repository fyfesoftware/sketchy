﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;

namespace FyfeSoftware.Sketchy.Core
{

    public enum SizeModeType
    {
        /// <summary>
        /// No sizing or moving
        /// </summary>
        None = 0,
        /// <summary>
        /// The size on the horizontal axis is locked
        /// </summary>
        Horizontal = 1,
        /// <summary>
        /// The size on the vertical axis is locked
        /// </summary>
        Vertical = 2,
        /// <summary>
        /// Don't show grip handles
        /// </summary>
        MoveOnly = 4,
        /// <summary>
        /// Only the Position and Position + Size grip handles
        /// </summary>
        LineMode = 8
    }

    /// <summary>
    /// A shape represents an abstract object that is placed onto the canvas and drawn 
    /// when the canvas is rendered. A shape is most usually a primitive shape (like a circle,
    /// square, rectangle, etc..)
    /// </summary>
    /// <remarks>A <see cref="T:IStencil"/> represents a collection of shapes that
    /// make up a complex shape on the screen</remarks>
    public interface IShape : IDrawable, IComponent, IDisposable
    {

        /// <summary>
        /// Arbritrary tagged data
        /// </summary>
        Object Tag { get; set; }

        /// <summary>
        /// Get the canvas this shape is present in
        /// </summary>
        /// <remarks>Exists because shapes can be nested within each other</remarks>
        ICanvas GetCanvas();

        /// <summary>
        /// Gets or sets the position of the shape within the drawing canvas
        /// </summary>
        PointF Position { get; set; }

        /// <summary>
        /// Gets or sets the size of the shape on the drawing canvas
        /// </summary>
        SizeF Size { get; set; }

        /// <summary>
        /// Gets the size of the shap on the canvas (includes zoom)
        /// </summary>
        SizeF DrawSize { get; }

        /// <summary>
        /// Gets the position on the canvas that this shape should be drawn at (includes zoom)
        /// </summary>
        PointF DrawPosition { get; }

        /// <summary>
        /// Gets the lock on this shape's size
        /// </summary>
        SizeModeType AllowedSizing { get; }

        /// <summary>
        /// Event is fired when the current shape needs to be refreshed
        /// </summary>
        event EventHandler RedrawRequired;

        /// <summary>
        /// Size has been changed
        /// </summary>
        event EventHandler SizeChanged;

        /// <summary>
        /// Position has been changed
        /// </summary>
        event EventHandler PositionChanged;

        /// <summary>
        /// Convert the point <paramref name="p"/> form a global canvas position to a client position
        /// </summary>
        Point PointToClientZoom(Point p);

        /// <summary>
        /// Convert the point <paramref name="p"/> form a global canvas position to a client position
        /// </summary>
        PointF PointToClientZoom(PointF p);
        
        /// <summary>
        /// Convert the point <paramref name="p"/> form a global canvas position to a client position
        /// </summary>
        Point PointToClient(Point p);

        /// <summary>
        /// Convert the point <paramref name="p"/> form a global canvas position to a client position
        /// </summary>
        PointF PointToClient(PointF p);

        /// <summary>
        /// Convert the point <paramref name="p"/> from a client position to a global canvas position
        /// </summary>
        PointF PointToCanvas(PointF p);

        /// <summary>
        /// Convert the point <paramref name="p"/> from a client position to a global canvas position
        /// </summary>
        Point PointToCanvas(Point p);

        /// <summary>
        /// Conver the point <paramref name="p"/> from a client position to a global canvas position taking zoom into account
        /// </summary>
        PointF PointToCanvasZoom(PointF p);

        /// <summary>
        /// Fires the redraw required event
        /// </summary>
        void OnRedrawRequired();

    }
}

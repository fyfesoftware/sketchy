﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace FyfeSoftware.Sketchy.Core.ComponentModel
{
    /// <summary>
    /// The shape site implements the ISite interface to link components to a container
    /// </summary>
    public class ShapeSite : ISite
    {

        [NonSerialized]
        private IContainer m_container;
        [NonSerialized]
        private IComponent m_component;

        /// <summary>
        /// Creates a new instance of the ShapeSite
        /// </summary>
        public ShapeSite() { }
        
        /// <summary>
        /// Creates a new instance of the ShapeSite site with the specified
        /// <paramref name="container"/> and <paramref name="component"/>
        /// </summary>
        /// <param name="container">The container that encapsulates the component</param>
        /// <param name="component">The component that is being encapsulated</param>
        public ShapeSite(IContainer container, IComponent component)
        {
            this.m_container = container;
            this.m_component = component;
        }

        #region ISite Members

        /// <summary>
        /// Gets the component (source) of this site association
        /// </summary>
        public IComponent Component { get { return this.m_component; } }

        /// <summary>
        /// Gets the container (target) of this site association
        /// </summary>
        public IContainer Container { get { return this.m_container; }}

        /// <summary>
        /// Gets a value that tells the component if the container is in design mode
        /// </summary>
        /// <remarks>Since Sketchy is a drawing framework, this always returns true</remarks>
        public bool DesignMode
        {
            get { return true; }
        }

        /// <summary>
        /// Gets or sets the name of the site
        /// </summary>
        public string Name { get; set; }


        #endregion

        #region IServiceProvider Members

        /// <summary>
        /// Get s service from the container. 
        /// </summary>
        /// <remarks>This container does not implement a get service method</remarks>
        public virtual object GetService(Type serviceType)
        {
            return null;
        }

        #endregion
    }
}

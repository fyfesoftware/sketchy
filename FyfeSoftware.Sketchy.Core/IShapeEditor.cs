﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// The IShapeEditor interface allows the developer of an editable shape to 
    /// specify a custom editor for the shape
    /// </summary>
    public interface IShapeEditor
    {

        /// <summary>
        /// Edit <paramref name="shape"/>
        /// </summary>
        /// <param name="shape">The shape to be edited</param>
        /// <returns>A state object that is to be interpreted by the shape object</returns>
        object EditShape(IEditableShape shape);

    }
}

﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core
{

    /// <summary>
    /// Represents the mouse buttons that are pressed
    /// </summary>
    public enum MouseKeys
    {
        None = 0,
        Left = 0x100000,
        Right = 0x200000,
        Middle = 0x400000
    }

    /// <summary>
    /// An IMouseInteractive object is an object that the user can use the mouse to interact with
    /// </summary>
    public interface IMouseInteractive
    {

        /// <summary>
        /// Handle the mouse click event
        /// </summary>
        void HandleMouseClick(Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse down event
        /// </summary>
        void HandleMouseDown(Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse up event
        /// </summary>
        void HandleMouseUp(Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse move event
        /// </summary>
        void HandleMouseMove(Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse double click event
        /// </summary>
        void HandleDoubleClick(Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse dwell event
        /// </summary>
        void HandleMouseDwell(Point p, MouseKeys mouseKeys);

    }
}

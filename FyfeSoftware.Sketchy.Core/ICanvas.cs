﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// A Canvas is a container for shape and stencil instances. The shape and stencil 
    /// instances participate in the canvas container.
    /// </summary>
    public interface ICanvas : IContainer, IMouseInteractive
    {
        /// <summary>
        /// Fired when the size of the canvas is changed
        /// </summary>
        event EventHandler SizeChanged;

        /// <summary>
        /// Send a shape to the back of the drawing stack
        /// </summary>
        void SendShapeToBack(IShape shape);

        /// <summary>
        /// Send the specified shape to the front of the drawing stack
        /// </summary>
        void BringShapeToFront(IShape shape);
        
        /// <summary>
        /// Find a shape at a particular position
        /// </summary>
        /// <param name="p">The point to locate the shape at</param>
        /// <param name="predicate">The predicate used to match the shapes</param>
        IMouseInteractiveShape FindShapeAt(Point p, Predicate<IShape> predicate);

        /// <summary>
        /// Find a named shape
        /// </summary>
        IShape FindShape(string name);

        /// <summary>
        /// Gets or sets the zoom factor of the canvas
        /// </summary>
        float Zoom { get; set; }

        /// <summary>
        /// The size of the canvas
        /// </summary>
        Size Size { get; set; }

        /// <summary>
        /// Gets or sets the minimum size of this canvas
        /// </summary>
        Size MinSize { get; set; }

        /// <summary>
        /// Draw the contents within <paramref name="viewWindow"/> of this canvas to <paramref name="g"/>
        /// </summary>
        /// <param name="g">The graphics context to draw to</param>
        /// <param name="viewWindow">The window to draw to</param>
        void DrawTo(Graphics g);

        /// <summary>
        /// Invalidate the canvas forcing a redraw
        /// </summary>
        void Invalidate();

        /// <summary>
        /// Clear the canvas
        /// </summary>
        void Clear();

        /// <summary>
        /// Signals to parent controls that a redraw of this component is required
        /// </summary>
        event EventHandler RedrawRequired;

        /// <summary>
        /// Signals that a child shape has been added to the canvas
        /// </summary>
        event EventHandler<StencilEventArgs> ShapeAdded;

        /// <summary>
        /// Signals that a child shape has been removed from the canvas
        /// </summary>
        event EventHandler<StencilEventArgs> ShapeRemoved;
    }
}

﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// Represents an shape that the user can interact with using the mouse
    /// </summary>
    public interface IMouseInteractiveShape : IMouseInteractive
    {
        /// <summary>
        /// A hit test is used to determine if the specified point is within the bounds
        /// of the current shape/
        /// </summary>
        /// <remarks>Used for determining if a mouse click happens in the context of the current
        /// shape</remarks>
        /// <param name="p">The point that is being tested</param>
        /// <returns>True if point <paramref name="p"/> lies within the boundaries of this shape</returns>
        bool HitTest(Point p);

        /// <summary>
        /// Handle a mouse leave the item
        /// </summary>
        void HandleMouseLeave(Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse enter event 
        /// </summary>
        void HandleMouseEnter(Point p, MouseKeys mouseKeys);

    }
}

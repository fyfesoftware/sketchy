﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core;

namespace FyfeSoftware.Sketchy.Core.Collections
{
    /// <summary>
    /// Identifies the type of modification
    /// </summary>
    public enum CollectionModificationType
    {
        ItemsAdded,
        ItemsRemoved,
        Cleared
    }

    /// <summary>
    /// Idenfies changes in the shape collection
    /// </summary>
    public class CollectionModifiedEventArgs : EventArgs
    {

        /// <summary>
        /// The type of modification that occurred
        /// </summary>
        public CollectionModificationType ModificationType { get; set; }

        /// <summary>
        /// Shapes added or removed from the collection
        /// </summary>
        public ICollection<IShape> Items { get; set; }

        /// <summary>
        /// Constructor for the modification event args
        /// </summary>
        public CollectionModifiedEventArgs(CollectionModificationType type, params IShape[] items)
        {
            this.ModificationType = type;
            this.Items = items;
        }

    }
}

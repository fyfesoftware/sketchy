﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core.Collections;

namespace FyfeSoftware.Sketchy.Core.Collections
{
    /// <summary>
    /// Represents a collection of shapes
    /// </summary>
    [Serializable]
    public class ShapeCollection : ICollection<IShape>
    {

        /// <summary>
        /// Fired when the collection has been modified
        /// </summary>
        public event EventHandler<CollectionModifiedEventArgs> CollectionModified;

        /// <summary>
        /// The internal collection of shapes
        /// </summary>
        private List<IShape> m_internalCollection = new List<IShape>(10);

        /// <summary>
        /// Add all items at once to the list
        /// </summary>
        public void AddRange(IEnumerable<IShape> shapes)
        {
            this.m_internalCollection.AddRange(shapes);
            if (CollectionModified != null)
                CollectionModified(this, new CollectionModifiedEventArgs(CollectionModificationType.ItemsAdded, shapes.ToArray()));

        }

        #region ICollection<IShape> Members

        /// <summary>
        /// Add a shape to the collection
        /// </summary>
        public void Add(IShape item)
        {
            this.m_internalCollection.Add(item);
            if (CollectionModified != null)
                CollectionModified(this, new CollectionModifiedEventArgs(CollectionModificationType.ItemsAdded, item));
        }

        /// <summary>
        /// Clear this shape collection
        /// </summary>
        public void Clear()
        {
            this.m_internalCollection.Clear();
            if (CollectionModified != null)
                CollectionModified(this, new CollectionModifiedEventArgs(CollectionModificationType.Cleared));
        }

        /// <summary>
        /// Returns true if the currnet collection contains <paramref name="item"/>
        /// </summary>
        public bool Contains(IShape item)
        {
            return this.m_internalCollection.Contains(item);
        }

        /// <summary>
        /// Copy this collection of shapes to an array of shapes
        /// </summary>
        public void CopyTo(IShape[] array, int arrayIndex)
        {
            this.m_internalCollection.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the count of shapes in this collection
        /// </summary>
        public int Count
        {
            get { return this.m_internalCollection.Count; }
        }

        /// <summary>
        /// Gets whether this collection is a readonly collection
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }
        
        /// <summary>
        /// Remove <paramref name="item"/> from this collection
        /// </summary>
        public bool Remove(IShape item)
        {
            bool result = this.m_internalCollection.Remove(item);
            if (result && CollectionModified != null)
                CollectionModified(this, new CollectionModifiedEventArgs(CollectionModificationType.ItemsRemoved, item));
            return result;
        }

        #endregion

        #region IEnumerable<IShape> Members

        /// <summary>
        /// Get the genericized enumerator for this collection
        /// </summary>
        public IEnumerator<IShape> GetEnumerator()
        {
            return this.m_internalCollection.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Get the non-genericized enumerator for this collection
        /// </summary>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}

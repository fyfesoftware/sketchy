﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// Provides data for an Interactive shape. This data includes a state object which represents
    /// the data provided by the IEditProvider after the edit has been accepted
    /// </summary>
    public class ShapeEditEventArgs : EventArgs
    {
        /// <summary>
        /// If true, cancels the edit action
        /// </summary>
        public bool Cancel { get; set; }

        /// <summary>
        /// Gets or sets the state object
        /// </summary>
        public object State { get; set; }

        /// <summary>
        /// The shape that is being edited
        /// </summary>
        public IInteractiveShape Shape { get; set; }
    }
}

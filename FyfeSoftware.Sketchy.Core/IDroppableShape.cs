﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core
{

    /// <summary>
    /// Identifies the effect to show to the user
    /// </summary>
    public enum DragEffect
    {
        NotAllowed,
        Allowed,
        NotSupported
    }

    /// <summary>
    /// Represents a shape that can have other shapes dropped onto it
    /// </summary>
    public interface IDroppableShape : IShape
    {

        /// <summary>
        /// Handles a scenario when another IDraggableShape instance is dragged over 
        /// the current instance
        /// </summary>
        /// <param name="p">The point where the shape is being dragged (relative to the current shape)</param>
        /// <param name="shape">The shape that is being dragged over this object</param>
        /// <returns>The effect to show to the user</returns>
        DragEffect HandleDragOver(IShape shape, Point p);
        
        /// <summary>
        /// Called by the canvas when <paramref name="shape"/> has been dropped onto the current
        /// instance at <paramref name="p"/>
        /// </summary>
        /// <param name="shape">The shape that was dropped onto this shape</param>
        /// <param name="p">The point within the current shape where <paramref name="shape"/> was dropped</param>
        void HandleDrop(IShape shape, Point p);
    }
}

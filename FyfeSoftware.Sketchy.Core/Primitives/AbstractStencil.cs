﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using FyfeSoftware.Sketchy.Core.ComponentModel;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core.Primitives
{
    /// <summary>
    /// Represents an abstract stencil object
    /// </summary>
    /// <remarks>You can think of a stencil as a "mini" canvas. The only difference is
    /// that a stencil's add and remove component methods do not trigger a redraw 
    /// as the stencil's draw method will iterate through the shapes when
    /// it needs to be redrawn.</remarks>
    [Serializable]
    public abstract class AbstractStencil : AbstractShape, IStencil
    {
        /// <summary>
        /// Shape components for this stencil shape
        /// </summary>
        [NonSerialized]
        private List<IComponent> m_shapeComponents= new List<IComponent>();

        /// <summary>
        /// Gets or sets the size of the stencil
        /// </summary>
        /// <remarks>Overidden so we can re-calc the stencil's child components' size
        /// and positions within this object</remarks>
        public override SizeF Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                // If the size has changed, we'll need to call the OnResize method
                if (!Size.Equals(value))
                    OnResize(value);

                // Now set the real size of this item
                base.Size = value;

            }
        }

        /// <summary>
        /// Do not allow sizing of stencils by default
        /// </summary>
        public override SizeModeType AllowedSizing
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// This abstract stencil has been resized
        /// </summary>
        /// <remarks>The default version of this method just scales everything up by the 
        /// precentage calculated</remarks>
        protected virtual void OnResize(SizeF newSize)
        {
            foreach (var component in m_shapeComponents)
            {
                IShape shapeComponent = component as IShape;
                if(shapeComponent == null) continue;

                // The size of the item as a percentage of the original
                SizeF pSize = new SizeF(shapeComponent.Size.Width / this.Size.Width,
                    shapeComponent.Size.Height / this.Size.Height);
                // The position of the item as a percentage of the height and width
                PointF pPosition = new PointF(shapeComponent.Position.X / this.Size.Width,
                    shapeComponent.Position.Y / this.Size.Height);

                // Now scale the thing up
                shapeComponent.Size = new SizeF(newSize.Width * pSize.Width, newSize.Height * pSize.Height);
                shapeComponent.Position = new PointF(newSize.Width * pPosition.X, newSize.Height * pPosition.Y);

            }
        }

        /// <summary>
        /// Override the draw method
        /// </summary>
        public override bool DrawTo(System.Drawing.Graphics g)
        {
            // We draw this stencil's shapes to a memory bitmap (all items are relative to this)
            float zoom = GetCanvas().Zoom;

            Bitmap bmp = new Bitmap((int)((this.Size.Width + 4) * zoom), (int)((this.Size.Height + 4) * zoom));
            Graphics slaveGraphics = Graphics.FromImage(bmp);
            
            // Draw each of the child components
            foreach (var component in m_shapeComponents)
            {
                IDrawable drawableComponent = component as IDrawable;
                if (drawableComponent != null)
                    drawableComponent.DrawTo(slaveGraphics);
            }

            // Now draw the slave graphics to the real graphics object
            g.DrawImage(bmp, DrawPosition.X , DrawPosition.Y );

            // Dispose the bitmap
            slaveGraphics.Dispose();
            bmp.Dispose();

            // Return true
            return true;
        }

        #region IStencil Members

        /// <summary>
        /// Gets the bounding box for this particular stencil object
        /// </summary>
        public System.Drawing.RectangleF BoundingBox
        {
            get { return new System.Drawing.RectangleF(Position, Size); }
        }

        #endregion

        #region IContainer Members

        /// <summary>
        /// Add a component to this stencil
        /// </summary>
        /// <param name="component">The component to add</param>
        /// <param name="name">A unique identifier for the component</param>
        public void Add(System.ComponentModel.IComponent component, string name)
        {

            // Ensure that we don't already have a component added
            int nCurrentShapes = m_shapeComponents.Count(o => o.Site != null && o.Site.Name.Equals(name));
            if (nCurrentShapes > 0)
                throw new SystemException(String.Format("The shape with name '{0}' already exists in this container", name));

            // Create the site
            ShapeSite site = new ShapeSite(this, component) { Name = name };
            component.Site = site;

            // Shape or decorator, decorators go to the back
            m_shapeComponents.Add(component);

            // Fire the shape added event
            if (component is IStencil && this.ShapeAdded != null)
                this.ShapeAdded(this, new StencilEventArgs() { Stencil = component as IStencil });

        }

        /// <summary>
        /// Add a component to the canvas
        /// </summary>
        /// <param name="component">The component to add</param>
        public void Add(System.ComponentModel.IComponent component)
        {

            if (component.Site == null)
                component.Site = new ShapeSite(this, component) { Name = Guid.NewGuid().ToString() };
            this.m_shapeComponents.Add(component);

            // Fire the shape added event
            if (component is IStencil && this.ShapeAdded != null)
                this.ShapeAdded(this, new StencilEventArgs() { Stencil = component as IStencil });

        }

        /// <summary>
        /// Gets the collection of components that are currently on this canvas
        /// </summary>
        public System.ComponentModel.ComponentCollection Components
        {
            get
            {
                return new ComponentCollection(m_shapeComponents.ToArray());
            }
        }

        /// <summary>
        /// Remove <paramref name="component"/> from the canvas
        /// </summary>
        /// <param name="component">The component to remove from the canvas</param>
        public void Remove(System.ComponentModel.IComponent component)
        {
            m_shapeComponents.Remove(component);

            // Fire the shape added event
            if (component is IStencil && this.ShapeRemoved != null)
                this.ShapeRemoved(this, new StencilEventArgs() { Stencil = component as IStencil });

        }


        /// <summary>
        /// Signal that a shape has been added
        /// </summary>
        public event EventHandler<StencilEventArgs> ShapeAdded;

        /// <summary>
        /// Signal that a shape has been removed
        /// </summary>
        public event EventHandler<StencilEventArgs> ShapeRemoved;

        #endregion
    }
}

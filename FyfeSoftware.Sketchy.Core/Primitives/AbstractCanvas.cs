﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using FyfeSoftware.Sketchy.Core.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace FyfeSoftware.Sketchy.Core.Primitives
{
    /// <summary>
    /// Represents an abstract implementation of a canvas. This serves as a basis for 
    /// deriving custom canvases.
    /// </summary>
    public abstract class AbstractCanvas : ICanvas
    {
        
        /// <summary>
        /// The current size of the canvas
        /// </summary>
        private Size m_size;

        /// <summary>
        /// True if a redraw is going to occur
        /// </summary>
        private bool m_pendingRedraw;

        ///// <summary>
        ///// The scroll position of this canvas
        ///// </summary>
        //private Rectangle m_viewPort;

        /// <summary>
        /// The current zoom level of the canvas
        /// </summary>
        private float m_zoom = 1.0f;

        #region ICanvas Members

        /// <summary>
        /// Invalidate
        /// </summary>
        public void Invalidate()
        {
            this.OnRedrawRequired();
        }

        /// <summary>
        /// Clear the canvas
        /// </summary>
        public void Clear()
        {
            while(this.m_shapeComponents.Count > 0)
                this.Remove(this.m_shapeComponents[0]);
        }

        /// <summary>
        /// Get or set the zoom of this canvas
        /// </summary>
        public float Zoom
        {
            get { return m_zoom; }
            set
            {
                m_zoom = value; OnRedrawRequired();
            }
        }


        /// <summary>
        /// Fired when the size of the canvas is changed
        /// </summary>
        public event EventHandler SizeChanged;

        ///// <summary>
        ///// Fired when the scroll position has changed
        ///// </summary>
        //public event EventHandler ScrollPositionChanged;

        ///// <summary>
        ///// The current viewport for this canvas
        ///// </summary>
        //public Rectangle ViewPort
        //{
        //    get
        //    {
        //        return m_viewPort;
        //    }
        //    set
        //    {
        //        bool equal = m_viewPort.Equals(value);
        //        m_viewPort = value;
        //        if (!equal && ScrollPositionChanged != null)
        //            ScrollPositionChanged(this, EventArgs.Empty);
        //        if(!equal) OnRedrawRequired();
        //    }
        //}

        /// <summary>
        /// The components in this canvas
        /// </summary>
        private List<IComponent> m_shapeComponents = new List<IComponent>();

        /// <summary>
        /// Gets or sets the minimum size of this canvas
        /// </summary>
        public Size MinSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the canvas
        /// </summary>
        public System.Drawing.Size Size
        {
            get
            { return m_size; }
            set
            {
                bool changed = !m_size.Equals(value);
                m_size = value;
                if (SizeChanged != null && changed)
                    SizeChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Send the specified shape to the back 
        /// </summary>
        public void SendShapeToBack(IShape shape)
        {
            if (m_shapeComponents.Remove(shape))
                m_shapeComponents.Insert(0, shape);
        }

        /// <summary>
        /// Bring the specified shape to the front
        /// </summary>
        public void BringShapeToFront(IShape shape)
        {
            if (m_shapeComponents.Remove(shape))
                m_shapeComponents.Add(shape);
        }

        /// <summary>
        /// Draw the contents of this canvas to <paramref name="g"/> using the viewport <paramref name="viewWindow"/>
        /// </summary>
        /// <param name="g">The graphics construct to draw to</param>
        public void DrawTo(System.Drawing.Graphics g)
        {
            // Maximum extent encountered
            SizeF maxExtent = SizeF.Empty;

            // We're drawing so this should be false
            m_pendingRedraw = false;

            // Draw each of the IDrawable objects in this current view window
            foreach (IComponent component in m_shapeComponents)
            {
                // Try to box into a drawable object
                IDrawable drawableComponent = component as IDrawable;
                IInteractiveShape interactiveShape = component as IInteractiveShape;
                IShape shapeComponent = component as IShape;

                if (drawableComponent != null) // the component is drawable so draw it if we can't determine if it is in this viewport
                    drawableComponent.DrawTo(g);

                // If the shape is outside of the bounds of this canvas, then we can expand it
                if (shapeComponent != null)
                {
                    // the extent of this object
                    PointF extent = shapeComponent.DrawPosition + shapeComponent.DrawSize;
                    // Increase the size if need be
                    maxExtent = new SizeF(extent.X  > maxExtent.Width ? extent.X  : maxExtent.Width,
                        extent.Y > maxExtent.Height ? extent.Y  : maxExtent.Height);
                }
            }

            // Set the size
            if (maxExtent.Height < MinSize.Height) maxExtent.Height = MinSize.Height;
            if (maxExtent.Width < MinSize.Width) maxExtent.Width = MinSize.Width;
            
            
            Size = new Size((int)maxExtent.Width, (int)maxExtent.Height);

        }

        /// <summary>
        /// Find a named shape
        /// </summary>
        public IShape FindShape(string name)
        {
            var shapes = from shapeComponent in m_shapeComponents
                         where shapeComponent is IShape &&
                         shapeComponent.Site != null &&
                         shapeComponent.Site.Name.Equals(name)
                         select shapeComponent as IShape;
            foreach (var shp in shapes)
                return shp;
            return null;
        }

        /// <summary>
        /// Find a shape at the specified point
        /// </summary>
        protected IMouseInteractiveShape FindShapeAt(Point p)
        {
            return FindShapeAt(p, null);
        }

        /// <summary>
        /// Locates a shape where the HitTest returns true at the specified point
        /// </summary>
        public IMouseInteractiveShape FindShapeAt(Point p, Predicate<IShape> predicate)
        {
            var shapes = from shape in m_shapeComponents
                         where shape is IMouseInteractiveShape &&
                        (shape as IMouseInteractiveShape).HitTest(p) &&
                        (predicate == null || predicate(shape as IShape))
                         orderby m_shapeComponents.IndexOf(shape) descending
                         select shape as IMouseInteractiveShape;

            // Return the first result
            foreach (var shp in shapes)
                return shp;
            return null; // Or return null
        }


        /// <summary>
        /// Signal that a shape has been added
        /// </summary>
        public event EventHandler<StencilEventArgs> ShapeAdded;

        /// <summary>
        /// Signal that a shape has been removed
        /// </summary>
        public event EventHandler<StencilEventArgs> ShapeRemoved;

        #endregion

        #region IContainer Members

        /// <summary>
        /// Add a component to the canvas
        /// </summary>
        /// <param name="component">The component to add</param>
        /// <param name="name">A unique identifier for the component</param>
        public virtual void Add(System.ComponentModel.IComponent component, string name)
        {

            // Ensure that we don't already have a component added
            int nCurrentShapes = m_shapeComponents.Count(o => o.Site != null && o.Site.Name.Equals(name));
            if (nCurrentShapes > 0)
                throw new SystemException(String.Format("The shape with name '{0}' already exists in this container", name));

            // Create the site
            ShapeSite site = new ShapeSite(this, component) { Name = name };
            component.Site = site;
            
            // Bind the child shape's redraw event to this shape
            if(component is IShape)
                (component as IShape).RedrawRequired += new EventHandler(AbstractCanvas_RedrawRequired);

            // Shape or decorator, decorators go to the back
            m_shapeComponents.Add(component);

            // Fire the shape added event
            if (this.ShapeAdded != null && component is IStencil)
                this.ShapeAdded(this, new StencilEventArgs() { Stencil = component as IStencil });


            // redraw
            OnRedrawRequired();
        }

        /// <summary>
        /// Signals that a redraw needs to be performed on this canvas
        /// </summary>
        public event EventHandler RedrawRequired;

        /// <summary>
        /// Handles the redraw required events coming from shapes on the canvas
        /// </summary>
        void AbstractCanvas_RedrawRequired(object sender, EventArgs e)
        {
            OnRedrawRequired();
        }

        /// <summary>
        /// Fires the redraw required event
        /// </summary>
        protected virtual void OnRedrawRequired()
        {
            if (this.RedrawRequired != null && !m_pendingRedraw)
                this.RedrawRequired(this, EventArgs.Empty);
        }

        /// <summary>
        /// Add a component to the canvas
        /// </summary>
        /// <param name="component">The component to add</param>
        public virtual void Add(System.ComponentModel.IComponent component)
        {

            // Create the site
            if (component.Site == null)
                component.Site = new ShapeSite(this, component) { Name = Guid.NewGuid().ToString() };

            // Bind the child shape's redraw event to this shape
            if (component is IShape)
                (component as IShape).RedrawRequired += new EventHandler(AbstractCanvas_RedrawRequired);

            // Shape or decorator, decorators go to the back
            m_shapeComponents.Add(component);

            // Fire the shape added event
            if (this.ShapeAdded != null && component is IStencil)
                this.ShapeAdded(this, new StencilEventArgs() { Stencil = component as IStencil });


            // redraw
            OnRedrawRequired();

        }

        /// <summary>
        /// Gets the collection of components that are currently on this canvas
        /// </summary>
        public System.ComponentModel.ComponentCollection Components
        {
            get
            {
                return new ComponentCollection(m_shapeComponents.ToArray());
            }
        }

        /// <summary>
        /// Remove <paramref name="component"/> from the canvas
        /// </summary>
        /// <param name="component">The component to remove from the canvas</param>
        public virtual void Remove(System.ComponentModel.IComponent component)
        {
            // Un-bind the redraw required event
            if (component is IShape)
                (component as IShape).RedrawRequired -= new EventHandler(this.AbstractCanvas_RedrawRequired);

            m_shapeComponents.Remove(component);

            // Fire the removed event
            if (this.ShapeRemoved != null && component is IStencil)
                this.ShapeRemoved(this, new StencilEventArgs() { Stencil = component as IStencil });


            // Redraw is required
            OnRedrawRequired();
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose this canvas
        /// </summary>
        public virtual void Dispose()
        {
            foreach (IComponent comp in m_shapeComponents)
                comp.Dispose();
            m_shapeComponents = null;
            GC.SuppressFinalize(this);
            
        }

        #endregion

        #region IMouseInteractive Members

        /// <summary>
        /// Handle a mouse click event at the specified point
        /// </summary>
        public abstract void HandleMouseClick(System.Drawing.Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse down event at the specified point
        /// </summary>
        public abstract void HandleMouseDown(System.Drawing.Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse up event at the specified point
        /// </summary>
        public abstract void HandleMouseUp(System.Drawing.Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse movement event at the specified point
        /// </summary>
        public abstract void HandleMouseMove(System.Drawing.Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse double click event at the specified point
        /// </summary>
        public abstract void HandleDoubleClick(System.Drawing.Point p, MouseKeys mouseKeys);

        /// <summary>
        /// Handle a mouse dwell event at the specified point
        /// </summary>
        public abstract void HandleMouseDwell(System.Drawing.Point p, MouseKeys mouseKeys);

        #endregion
    }
}

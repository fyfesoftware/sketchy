﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;

namespace FyfeSoftware.Sketchy.Core.Primitives
{
    /// <summary>
    /// Ths AbstractRectangleShape partially implements the IShape class to represent a
    /// shape on the user's screen
    /// </summary>
    [Serializable]
    public abstract class AbstractShape : IShape
    {
        #region IShape Members

        /// <summary>
        /// Position of this item in the canvas space
        /// </summary>
        protected PointF m_position = PointF.Empty;
        /// <summary>
        /// Size of the this shape in the canvas space
        /// </summary>
        protected SizeF m_size = SizeF.Empty;

        /// <summary>
        /// Gets or sets the position of the top-left corner of the shape
        /// </summary>
        public virtual System.Drawing.PointF Position { 
            get { return m_position; }
            set
            {
                bool equal = value.Equals(m_position);
                m_position = value;
                if (!equal && PositionChanged != null)
                    PositionChanged(this, EventArgs.Empty);
                this.OnRedrawRequired();
            }
        }

        /// <summary>
        /// Gets or sets the size of the shape
        /// </summary>
        public virtual System.Drawing.SizeF Size { get { return m_size; }
            set 
            {
                bool equal = m_size.Equals(value);
                m_size = value;
                if (!equal && SizeChanged != null)
                    SizeChanged(this, EventArgs.Empty);
                this.OnRedrawRequired();
            }
        }

        /// <summary>
        /// Gets the lock on the shape's size
        /// </summary>
        public virtual SizeModeType AllowedSizing { get { return SizeModeType.Horizontal | SizeModeType.Vertical; } }

        /// <summary>
        /// Fires an event that informs the canvas that a redraw is required
        /// </summary>
        public event EventHandler RedrawRequired;

        /// <summary>
        /// The size has been changed
        /// </summary>
        public event EventHandler SizeChanged;

        /// <summary>
        /// The position has been changed
        /// </summary>
        public event EventHandler PositionChanged;

        /// <summary>
        /// Tagged data
        /// </summary>
        public Object Tag { get; set; }

        /// <summary>
        /// Conver the point <paramref name="p"/> form a canvas to a client position factoring zoom
        /// </summary>
        /// <remarks>Converts a point that is relative to canvas 0,0 to a point that is relative to this
        /// shape's position</remarks>
        public System.Drawing.Point PointToClientZoom(System.Drawing.Point p)
        {
            float zoom = GetCanvas().Zoom;
            Point retVal = new Point((int)(p.X / zoom), (int)(p.Y / zoom));
            retVal.Offset(-(int)Position.X, -(int)Position.Y);
            return retVal;
        }

        /// <summary>
        /// Conver the point <paramref name="p"/> form a canvas to a client position factoring zoom
        /// </summary>
        /// <remarks>Converts a point that is relative to canvas 0,0 to a point that is relative to this
        /// shape's position</remarks>
        public System.Drawing.PointF PointToClientZoom(System.Drawing.PointF p)
        {
            float zoom = GetCanvas().Zoom;
            PointF retVal = new PointF(p.X / zoom, p.Y / zoom);
            retVal.X -= Position.X; retVal.Y -= Position.Y;
            return retVal;
        }

        /// <summary>
        /// Conver the point <paramref name="p"/> form a canvas to a client position
        /// </summary>
        /// <remarks>Converts a point that is relative to canvas 0,0 to a point that is relative to this
        /// shape's position</remarks>
        public System.Drawing.Point PointToClient(System.Drawing.Point p)
        {
            Point retVal = p;
            retVal.Offset(-(int)Position.X, -(int)Position.Y);
            return retVal;
        }

        /// <summary>
        /// Conver the point <paramref name="p"/> form a canvas to a client position
        /// </summary>
        /// <remarks>Converts a point that is relative to canvas 0,0 to a point that is relative to this
        /// shape's position</remarks>
        public System.Drawing.PointF PointToClient(System.Drawing.PointF p)
        {
            PointF retVal = p;
            retVal.X -= Position.X; retVal.Y -= Position.Y ;
            return retVal;
        }

        /// <summary>
        /// Convert the point <paramref name="p"/> form a client position to a canvas position
        /// </summary>
        public System.Drawing.Point PointToCanvas(System.Drawing.Point p)
        {
            Point retVal = p;
            retVal.Offset((int)Position.X, (int)Position.Y);
            return retVal;
        }

        /// <summary>
        /// Convert the point <paramref name="p"/> form a client position to a canvas position
        /// </summary>
        public System.Drawing.PointF PointToCanvas(System.Drawing.PointF p)
        {
            PointF retVal = p;
            retVal.X += Position.X; retVal.Y += Position.Y;
            return retVal;
        }

        /// <summary>
        /// Convert the point <paramref name="p"/> form a client position to a canvas position
        /// </summary>
        public System.Drawing.PointF PointToCanvasZoom(System.Drawing.PointF p)
        {
            float zoom = GetCanvas().Zoom;
            PointF retVal = p;
            retVal.X += Position.X / zoom; retVal.Y += Position.Y / zoom;
            return retVal;
        }

        /// <summary>
        /// Gets the size this shape will drawn on the canvas including zoom
        /// </summary>
        public virtual SizeF DrawSize { get { float zoom = GetCanvas().Zoom; return new SizeF(Size.Width * zoom, Size.Height * zoom); } }

        /// <summary>
        /// Gets the position this shape will drawn on the canvas including zoom
        /// </summary>
        public virtual PointF DrawPosition
        {
            get
            {
                float zoom = GetCanvas().Zoom; 
                //Rectangle scrollPosition = this.Site.Container is IStencil ? Rectangle.Empty : GetCanvas().ViewPort; 
                //return new PointF(Position.X * zoom - scrollPosition.X, Position.Y * zoom - scrollPosition.Y);
                return new PointF(Position.X * zoom , Position.Y * zoom );
            }
        }

        /// <summary>
        /// Fires the redraw required event
        /// </summary>
        public virtual void OnRedrawRequired()
        {
            if (this.RedrawRequired != null)
                this.RedrawRequired(this, EventArgs.Empty);
        }

        /// <summary>
        /// Get the canvas this shape belongs to
        /// </summary>
        public ICanvas GetCanvas()
        {
            // No site = no container
            if(this.Site == null)
                return null;

            // Get the top level canvas
            IContainer cntr = this.Site.Container;
            while (!(cntr is ICanvas) && cntr != null)
            {
                IComponent cntrComponent = cntr as IComponent;
                if (cntrComponent != null && cntrComponent.Site != null)
                    cntr = cntrComponent.Site.Container;
                else
                    return null;
            }

            // Return the container
            return cntr as ICanvas;
        }

        #endregion

        #region IDrawable Members

        /// <summary>
        /// Draws this shape to the specified graphics device
        /// </summary>
        public abstract bool DrawTo(System.Drawing.Graphics g);

        #endregion

        #region IComponent Members

        /// <summary>
        /// The disposed event handler
        /// </summary>
        public event EventHandler Disposed;

        /// <summary>
        /// Gets or sets the site (canvas site usually) that links this shape to a canvas
        /// </summary>
        public virtual System.ComponentModel.ISite Site { get; set; }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose of this shape
        /// </summary>
        public virtual void Dispose()
        {
            // Un-register from the container site if possible
            if (this.Site != null)
                this.Site.Container.Remove(this);

            if (Disposed != null)
                Disposed(this, EventArgs.Empty);
        }

        #endregion
    }
}

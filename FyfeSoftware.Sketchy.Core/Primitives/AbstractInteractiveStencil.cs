﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core.Primitives;
using FyfeSoftware.Sketchy.Core;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core.Primitives.Stencils
{
    /// <summary>
    /// Represents an abstract interactive stencil
    /// </summary>
    [Serializable]
    public abstract class AbstractInteractiveStencil : AbstractStencil, IInteractiveShape
    {
        #region IInteractiveShape Members

        /// <summary>
        /// A shape that decorates this shape
        /// </summary>
        [NonSerialized]
        private IDecorator m_decorator;

        /// <summary>
        /// Get or set the decorator 
        /// </summary>
        public virtual IDecorator Decorator
        {
            get { return m_decorator; }
            set
            {
                // Is the current decorator set? if so kill it
                if (m_decorator != null)
                    m_decorator.Dispose();

                m_decorator = value;

                // Assign the decorator
                if (m_decorator != null)
                    m_decorator.Decorates = this;

                OnRedrawRequired();
            }
        }

        #endregion

        #region IDroppableShape Members

        /// <summary>
        /// Handle the event that occurs when a shape is dragged over this shape
        /// </summary>
        public virtual DragEffect HandleDragOver(IShape shape, System.Drawing.Point p)
        {
            IConnector connectorShape = shape as IConnector;
            if (connectorShape != null && (connectorShape.IsConnectionAllowed(this, connectorShape.Target) || connectorShape.IsConnectionAllowed(connectorShape.Source, this)))
                return DragEffect.Allowed;
            return DragEffect.NotAllowed;
                
        }

        /// <summary>
        /// Handle the event that occurs when a shape is dropped onto this shape
        /// </summary>
        /// <remarks>Default action is no action</remarks>
        public virtual void HandleDrop(IShape shape, System.Drawing.Point p)
        {
            // Handle the connector shape and/or notes
            IConnector connectorShape = shape as IConnector;
            if (connectorShape.IsConnectionAllowed(this, connectorShape.Target))
                connectorShape.Source = this;
            if (connectorShape.IsConnectionAllowed(connectorShape.Source, this))
                connectorShape.Target = this;            
        }

        #endregion

        #region IEditableShape Members

        /// <summary>
        /// Gets or sets the editor to use for this stencil
        /// </summary>
        public abstract IShapeEditor Editor { get; }

        /// <summary>
        /// Handle the event that occurs when the user is finished editing
        /// </summary>
        public abstract void HandleEndEdit(ShapeEditEventArgs e);

        #endregion

        #region IMouseInteractiveShape Members

        /// <summary>
        /// Perform a hit test on this object
        /// </summary>
        public virtual bool HitTest(System.Drawing.Point p)
        {
            float zoom = GetCanvas().Zoom;
            return new RectangleF(DrawPosition, DrawSize).Contains(p);
        }

        /// <summary>
        /// Handle the mouse leaving the object
        /// </summary>
        public virtual void HandleMouseLeave(System.Drawing.Point p, MouseKeys mouseKeys) { }

        /// <summary>
        /// Handle the mouse entering this object
        /// </summary>
        public virtual void HandleMouseEnter(System.Drawing.Point p, MouseKeys mouseKeys) { }

        #endregion

        #region IMouseInteractive Members
        /// <summary>
        /// Handle a mouse clicking the object
        /// </summary>
        public virtual void HandleMouseClick(System.Drawing.Point p, MouseKeys mouseKeys) { }

        /// <summary>
        /// Handle the mouse being depressed on this object
        /// </summary>
        public virtual void HandleMouseDown(System.Drawing.Point p, MouseKeys mouseKeys) { }

        /// <summary>
        /// Handle the mouse being released on this object
        /// </summary>
        public virtual void HandleMouseUp(System.Drawing.Point p, MouseKeys mouseKeys) { }

        /// <summary>
        /// Handle the mouse moving on this object
        /// </summary>
        public virtual void HandleMouseMove(System.Drawing.Point p, MouseKeys mouseKeys) { }

        /// <summary>
        /// Handle a double click event on this object
        /// </summary>
        public virtual void HandleDoubleClick(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            if (Editor != null)
                Editor.EditShape(this);
        }

        /// <summary>
        /// Handle the mouse dwelling on this object
        /// </summary>
        public virtual void HandleMouseDwell(System.Drawing.Point p, MouseKeys mouseKeys)
        {
            
        }

        #endregion
    }
}

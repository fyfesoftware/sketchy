﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// Directions that a connector can be facing
    /// </summary>
    public enum DirectionType
    {
        /// <summary>
        /// The connector is facing north
        /// </summary>
        North,
        /// <summary>
        /// The connector is facing east
        /// </summary>
        East,
        /// <summary>
        /// The connector is facing south
        /// </summary>
        South,
        /// <summary>
        /// The connector is facing west
        /// </summary>
        West
    }

    /// <summary>
    /// An IConnector represents an  object that connects one or more
    /// <see cref="T:IShape"/> instances together
    /// </summary>
    public interface IConnector
    {

        /// <summary>
        /// The target of the connector
        /// </summary>
        IShape Target { get; set; }

        /// <summary>
        /// The source of the connector
        /// </summary>
        IShape Source { get; set; }

        /// <summary>
        /// The direction of this connector
        /// </summary>
        DirectionType Direction { get; set; }

        /// <summary>
        /// Fired when the connection changes
        /// </summary>
        event EventHandler ConnectionChanged;

        /// <summary>
        /// Determines if the connector configuration is allowed
        /// </summary>
        /// <param name="source">The source of the connection</param>
        /// <param name="target">The target of the connection</param>
        /// <returns>True if the connection is possible</returns>
        bool IsConnectionAllowed(IShape source, IShape target);

        /// <summary>
        /// Allows the connector to recalculate its position
        /// </summary>
        void RecalculatePosition();
    }
}

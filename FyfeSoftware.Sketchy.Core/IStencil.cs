﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace FyfeSoftware.Sketchy.Core
{
    /// <summary>
    /// A stencil represents a collection of primitive <see cref="T:IShape"/> objects that 
    /// represent a cohesive modelling concept. An example of a stencil would be the "class"
    /// shape in UML
    /// </summary>
    public interface IStencil : IShape, IContainer
    {

        /// <summary>
        /// Gets the calculated bounding box for the stencil
        /// </summary>
        RectangleF BoundingBox { get; }

        /// <summary>
        /// Signals that a child shape has been added to the canvas
        /// </summary>
        event EventHandler<StencilEventArgs> ShapeAdded;

        /// <summary>
        /// Signals that a child shape has been removed from the canvas
        /// </summary>
        event EventHandler<StencilEventArgs> ShapeRemoved;
    }
}

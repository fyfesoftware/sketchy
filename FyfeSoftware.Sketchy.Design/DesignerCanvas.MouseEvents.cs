﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core.Primitives;
using FyfeSoftware.Sketchy.Core;
using System.Drawing;
using System.Timers;
using System.Diagnostics;
using FyfeSoftware.Sketchy.Core.Shapes;

namespace FyfeSoftware.Sketchy.Design
{
    /// <summary>
    /// The HL7StaticModelDesignerCanvas represents a canvas that is used to 
    /// design HL7v3 models on.
    /// </summary>
    public partial class DesignerCanvas 
    {

        /// <summary>
        /// Contains the last "hit" item 
        /// </summary>
        private IMouseInteractiveShape m_lastHitItem = null;

        /// <summary>
        /// Contains the last reported cursor position
        /// </summary>
        private Point m_lastReportedCursorPosition = Point.Empty; 

        /// <summary>
        /// A timer that is used to determine if a mouse dwell should occur
        /// </summary>
        private Timer m_dwellTimer = new Timer();

        /// <summary>
        /// Gets or sets the amount of time that the user must dwell to trigger a "dwell" event
        /// </summary>
        public int DwellTime { get { return (int)m_dwellTimer.Interval; } set { m_dwellTimer.Interval = Convert.ToDouble(value); } }

        /// <summary>
        /// Handle a mouse click event
        /// </summary>
        public override void HandleMouseClick(Point p, MouseKeys mouseKeys)
        {
            IMouseInteractiveShape currentShape = FindShapeAt(p);

            if (currentShape != null) // There was a shape at this position
            {
                // Select the shape
                if (currentShape is IInteractiveShape)
                    SelectShape(currentShape as IInteractiveShape);
            }
            
        }

        /// <summary>
        /// Handle a mouse down event
        /// </summary>
        public override void HandleMouseDown(Point p, MouseKeys mouseKeys)
        {
            IMouseInteractiveShape currentShape = FindShapeAt(p);

            // Is the current shape not 
            if (currentShape != null) // There was a shape at this position
            {
                if (!this.SelectedShapes.Contains(currentShape as IShape) && 
                    !(currentShape is IDecorator) &&
                    currentShape is IDecoratableShape)
                {
                    this.ClearSelection();
                    this.SelectShape(currentShape as IInteractiveShape);
                    var decorator = (currentShape as IDecoratableShape).Decorator as IMouseInteractiveShape;
                    if (decorator != null)
                    {
                        currentShape = decorator;
                        this.m_lastHitItem = currentShape;
                        this.BringShapeToFront(decorator as IShape);
                    }
                }

                currentShape.HandleMouseDown(p, mouseKeys);
            }
            else
            {
                // Are we currently in select mode?
                IShape selectionShape = this.FindShape("__selectionShape");
                if (selectionShape != null) // selection shape exists, remove it
                    this.Remove(selectionShape);

                // Create the selection shape
                selectionShape = new CanvasSelectionShape(p);
                // Add to the shape collection
                this.Add(selectionShape, "__selectionShape");
            }
        }

        /// <summary>
        /// Handle a mouse up event
        /// </summary>
        public override void HandleMouseUp(Point p, MouseKeys mouseKeys)
        {
            IMouseInteractiveShape currentShape = FindShapeAt(p);
            IShape selectionShape = this.FindShape("__selectionShape");

            if (currentShape != null && selectionShape == null) // There was a shape at this position
                currentShape.HandleMouseUp(p, mouseKeys);
            else
            {
                // Are we currently in select mode?
                if (selectionShape != null) // selection shape exists, remove it
                {
                    // Select all items in the binding box
                    SelectShapes((selectionShape as CanvasSelectionShape).BindingBox);
                    this.Remove(selectionShape);
                }
            }
        }

        /// <summary>
        /// Handle a mouse move event
        /// </summary>
        public override void HandleMouseMove(Point p, MouseKeys mouseKeys)
        {
            // First off, reset the dwell timer
            m_dwellTimer.Stop();

            IMouseInteractiveShape currentShape = FindShapeAt(p);

            // Set the last reported cursor position
            m_lastReportedCursorPosition = p;

            if(currentShape != null && currentShape == m_lastHitItem) // The mouse is still inside of the same item, it is a mouse move
                currentShape.HandleMouseMove(p, mouseKeys);
            else if (currentShape != m_lastHitItem) // the current item has changed
            {
                if (currentShape != null)
                    currentShape.HandleMouseEnter(p, mouseKeys);
                if (m_lastHitItem != null)
                    m_lastHitItem.HandleMouseLeave(p, mouseKeys);
            }

            if (mouseKeys == MouseKeys.None) // No keys are depressed, we'll start the dwell cycle 
                m_dwellTimer.Start();
            else // The mouse has been moved with a key depressed
            {
                // TODO: Drag condition
                // TODO: Selection condition
                
                // Set the size of the sel shape
                IShape selectionShape = this.FindShape("__selectionShape");
                if (selectionShape != null)
                    selectionShape.Size = new Size(selectionShape.PointToClient(p));
            }

            // Set the last hit item
            m_lastHitItem = currentShape;

        }

        /// <summary>
        /// Handle a mouse double click event
        /// </summary>
        public override void HandleDoubleClick(Point p, MouseKeys mouseKeys)
        {
            IMouseInteractiveShape currentShape = FindShapeAt(p);

            if (currentShape != null) // There was a shape at this position
                currentShape.HandleDoubleClick(p, mouseKeys);
        }

        /// <summary>
        /// Handle a mouse dwell event
        /// </summary>
        public override void HandleMouseDwell(Point p, MouseKeys mouseKeys)
        {
            IMouseInteractiveShape currentShape = FindShapeAt(p);

            if (currentShape != null) // There was a shape at this position
                currentShape.HandleMouseDwell(p, mouseKeys);
            
        }

        /// <summary>
        /// Initializes mouse events
        /// </summary>
        private void InitializeMouseEvents()
        {
            m_dwellTimer = new Timer(1000d);
            m_dwellTimer.AutoReset = false;
            m_dwellTimer.Elapsed += new ElapsedEventHandler(m_dwellTimer_Elapsed);
        }

        /// <summary>
        /// Handler for the m_dwellTimer elapsed event
        /// </summary>
        private void m_dwellTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Is the last hit item still valid?
            if (m_lastHitItem != null)
                m_lastHitItem.HandleMouseDwell(m_lastReportedCursorPosition, MouseKeys.None);
        }


    }
}

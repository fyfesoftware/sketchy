﻿/* 
 * Fyfe Software Sketchy Designer Toolkit 
 * Copyright 2014-2015 Fyfe Software Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you 
 * may not use this file except in compliance with the License. You may 
 * obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations under 
 * the License.
 * 
 * Author: Justin
 * Date: 4-16-2015
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FyfeSoftware.Sketchy.Core.Collections;
using FyfeSoftware.Sketchy.Core;
using FyfeSoftware.Sketchy.Core.Decorators;
using System.Drawing;
using System.ComponentModel;

namespace FyfeSoftware.Sketchy.Design
{
    /// <summary>
    /// Selection manager for the HL7v3 Static Model Designer canvas
    /// </summary>
    public partial class DesignerCanvas 
    {
        /// <summary>
        /// Gets a list of shapes that have been selected
        /// </summary>
        public ShapeCollection SelectedShapes { get; private set; }

        /// <summary>
        /// Initialize the selection stuff
        /// </summary>
        private void InitializeSelection()
        {
            SelectedShapes = new ShapeCollection();
            SelectedShapes.CollectionModified += new EventHandler<CollectionModifiedEventArgs>(SelectedShapes_CollectionModified);
        }

        /// <summary>
        /// Selects <paramref name="shape"/>
        /// </summary>
        private void SelectShape(IInteractiveShape shape)
        {
            // Clear the selected shapes
            ClearSelection();
            SelectedShapes.Add(shape);
        }

        /// <summary>
        /// Clear the current selection
        /// </summary>
        public void ClearSelection()
        {
            foreach (IInteractiveShape shape in SelectedShapes)
                if(shape.Decorator != null)
                    shape.Decorator.Dispose();
            if(SelectedShapes.Count > 0)
                SelectedShapes.Clear();
        }

        /// <summary>
        /// Select all shapes in the <paramref name="rect"/>
        /// </summary>
        public void SelectShapes(RectangleF rect)
        {
            var shapes = from IComponent shape in Components
                         where shape is IInteractiveShape &&
                         rect.Contains(new RectangleF((shape as IShape).Position, (shape as IShape).Size))
                         select shape as IInteractiveShape;
            ClearSelection(); // Cleear selection
            SelectedShapes.AddRange(shapes);

        }

        /// <summary>
        /// The selected shapes collection has been modified
        /// </summary>
        private void SelectedShapes_CollectionModified(object sender, CollectionModifiedEventArgs e)
        {
            foreach (IInteractiveShape shape in SelectedShapes)
            {
                BringShapeToFront(shape);
                shape.Decorator = new SelectionDecorator() { SiblingSelected = SelectedShapes };
            }
            
        }

    }
}

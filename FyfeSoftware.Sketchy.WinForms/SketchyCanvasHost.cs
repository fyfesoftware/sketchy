﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FyfeSoftware.Sketchy.Core;

namespace FyfeSoftware.Sketchy.WinForms
{
    /// <summary>
    /// Represents a user control that hosts a sketchy canvas
    /// </summary>
    public partial class SketchyCanvasHost : UserControl
    {

        /// <summary>
        /// The canvas this control displays
        /// </summary>
        private ICanvas m_canvas;

        /// <summary>
        /// Gets or sets the sketchy canvas that this control hosts
        /// </summary>
        public ICanvas Canvas
        {
            get
            {
                return m_canvas;
            }
            set
            {
                // Clear the existing canvas
                if (m_canvas != null)
                {
                    m_canvas.SizeChanged -= new EventHandler(m_canvas_SizeChanged);
                    m_canvas.RedrawRequired -= new EventHandler(m_canvas_RedrawRequired);
                }

                // Set 
                m_canvas = value;
                
                if (m_canvas != null)
                {
                    m_canvas.RedrawRequired += new EventHandler(m_canvas_RedrawRequired);
                    m_canvas.SizeChanged += new EventHandler(m_canvas_SizeChanged);
                    m_canvas.MinSize = this.Size;
                }
            }
        }


        /// <summary>
        /// Size was changed
        /// </summary>
        private void m_canvas_SizeChanged(object sender, EventArgs e)
        {
            //// Set the min max on the scroll bars
            //if (Canvas != null)
            //{
            //    int maxV = Canvas.Size.Height - pbMain.Size.Height + 10,
            //        maxH = Canvas.Size.Width - pbMain.Size.Width + 10; // Calc max H max W

            //    // Set visibility
            //    hsBar.Visible = maxH > 0;
            //    vsPanel.Visible = maxV > 0;

            //    // Set max
            //    if (maxH > 0)
            //    {
            //        hsBar.Maximum = maxH;
            //        hsBar.LargeChange = (int)(maxH / 4);
            //        hsBar.SmallChange = (int)(maxH / 10);
            //    }
            //    if (maxV > 0)
            //    {
            //        vsBar.Maximum = maxV;
            //        vsBar.SmallChange = (int)(maxV / 10);
            //        vsBar.LargeChange = (int)(maxV / 4);
            //    }
            //}
            if (Canvas != null)
                pbMain.Size = Canvas.Size;
        }

        /// <summary>
        /// Redraw is required
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void  m_canvas_RedrawRequired(object sender, EventArgs e)
        {
            pbMain.Invalidate();
        } 
        
        public SketchyCanvasHost()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
        }

        /// <summary>
        /// Paint the picture
        /// </summary>
        private void pbMain_Paint(object sender, PaintEventArgs e)
        {
            if (Canvas != null)
                Canvas.DrawTo(e.Graphics);

        }

        /// <summary>
        /// Mouse has been clicked
        /// </summary>
        private void pbMain_MouseClick(object sender, MouseEventArgs e)
        {
            if (Canvas != null)
                Canvas.HandleMouseClick(new Point(e.X, e.Y), (MouseKeys)e.Button);
        }

       
        /// <summary>
        /// Mouse has been double clicked
        /// </summary>
        private void pbMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
            if (Canvas != null)
                Canvas.HandleDoubleClick(new Point(e.X, e.Y), (MouseKeys)e.Button);
        }

        /// <summary>
        /// Mouse has been pushed down
        /// </summary>
        private void pbMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (Canvas != null)
                Canvas.HandleMouseDown(new Point(e.X, e.Y), (MouseKeys)e.Button);
        }

        /// <summary>
        /// Mouse has been moved
        /// </summary>
        private void pbMain_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (Canvas != null)
                Canvas.HandleMouseMove(new Point(e.X, e.Y), (MouseKeys)e.Button);
        }

        /// <summary>
        /// Mouse has been released
        /// </summary>
        private void pbMain_MouseUp(object sender, MouseEventArgs e)
        {
            
            if (Canvas != null)
                Canvas.HandleMouseUp(new Point(e.X, e.Y), (MouseKeys)e.Button);
        }

        /// <summary>
        /// Canvas size
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SketchyCanvasHost_SizeChanged(object sender, EventArgs e)
        {
            if (Canvas != null)
            {
                Canvas.MinSize = this.Size;
                this.pbMain.MinimumSize = this.Size;                
            }
        }

     
       
    }
}
